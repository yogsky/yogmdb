const request = require('request');
const cheerio = require('cheerio');

const generateUrl = (id) => `http://www.imdb.com/title/${id}`;

const mapper = (element, idx, rating) => ({
  title: element.attribs.title,
  imdbID: element.parent.parent.attribs['data-tconst'],
  poster: element.attribs.loadlate,
  imdbRating: Number(rating)
});


const transform = (body) => {
  const $ = cheerio.load(body);
  const ratings = Array.from($('.rating.rating-list .rating-rating .value')).map(x => x.children[0].data);
  return Array.from($('.rec_slide .rec_item img'))
  .map((item, idx) => mapper(item, idx, ratings[idx]));
};


const yogScraper = (title) => new Promise((resolve, reject) => {
  const uri = generateUrl(title.imdbID);
  request(uri, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      title.children = transform(body);
      resolve(title);
    } else {
      reject(error);
    }
  });
})

module.exports = yogScraper;
