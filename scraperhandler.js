const yogScraper = require('./scraper');
const requestTitle = require('./requesttitle');

const mayLikesCache = {};

const omdbToImdbMapper = (obj) => ({
  title: obj.Title,
  poster: obj.Poster,
  imdbID: obj.imdbID,
  imdbRating: obj.imdbRating,
});

const scraperHandler = (query) => {
  return new Promise((resolve, reject) => {
    const { imdbID } = query;
    //maybe its cached ?
    if(mayLikesCache[imdbID]) {
      console.log('found in mayLikesCache');
      resolve(mayLikesCache[imdbID]);
      return;
    }

    console.log('scraping');
    let master;
    requestTitle(query)
      .then((title) => {
        master = omdbToImdbMapper(title);
        return yogScraper(master);
      })
      .then(initialScrapes => Promise.all(initialScrapes.children.map(movieObj => yogScraper(movieObj))))
      .then(reducedScrapes => {
        master.children = reducedScrapes;
        mayLikesCache[imdbID] = master;
        console.log('finished scraping, adding to mayLikesCache');
        resolve(mayLikesCache[imdbID]);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      })
  })
}


module.exports = scraperHandler;
