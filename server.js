const express = require('express');
const path = require('path');
const requestTitle = require('./requesttitle');
const scraperHandler = require('./scraperhandler');

const app = express();

app.use('/', express.static(__dirname));

app.get('/yog/title', (req, res) => {
  requestTitle(req.query)
    .then((title) => res.send(title).status(200))
    .catch((error) => res.send(error).status(500))
});

app.get('/yog/maylikes', (req, res) => {
  scraperHandler(req.query)
    .then((maylike) => res.send(maylike).status(200))
    .catch((error) => res.send(error).status(500))
});

app.use('*', (req, res) => {
  res.sendFile(path.resolve('./index.html'), 200);
});

const port = process.env.PORT || 1337;

app.listen(port);

console.log(`YogServer is live @ port ${port}`)
