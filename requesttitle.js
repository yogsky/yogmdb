const request = require('request');

const urlByTitle = (title, type) => `https://www.omdbapi.com/?t=${title}&y=&plot=full&type=${type}&apikey=123479d1&r=json`;
const urlById = (id) => `https://www.omdbapi.com/?i=${id}&apikey=123479d1&r=json`;

const titlesCache = {};

const reqTitle = (input) => {
  return new Promise((resolve, reject) => {
    if(titlesCache[input.imdbID]) {
      resolve(titlesCache[input.imdbID]);
      return;
    }
    const url = input.imdbID ? urlById(input.imdbID) : urlByTitle(input.title, input.type);
    request(url, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        const title = JSON.parse(body);
        if(!title.Error) {
          titlesCache[title.imdbID] = title;
          resolve(title);
        } else {
          reject(title);
        }
      } else {
        reject(error);
      }
    });
  });
}


module.exports = reqTitle;
