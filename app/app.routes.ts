import { Routes, RouterModule }   from '@angular/router';
import { MainComponent } from './main/main';
import { MovieComponentContainer } from './movie/moviecontainer/moviecontainer';
import { TicTacToeComponent } from './tictactoe/tic';

const routes: Routes = [
    { path: 'main', component: MainComponent },
    { path: 'movies', component: MovieComponentContainer },
    { path: 'tic', component: TicTacToeComponent },
    { path: '**', component: MainComponent }
];

export const router = RouterModule.forRoot(routes);
