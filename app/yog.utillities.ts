export class YogUtillities {
    static getRandomInt = (min: number, max: number): number => Math.floor(Math.random() * (max - min + 1)) + min;
    static throttle = (fn: Function, limit: number) => {
        let wait = false;
        return () => {
            if (!wait) {
                fn.call(null);
                wait = true;
                setTimeout(() => {
                    wait = false;
                }, limit);
            }
        };
    };
}
