import { Component, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { RouterLink } from '@angular/router';

const SOURCE_CODE_FLASH_INTERVAL = 500;

@Component({
    selector: 'main',
    providers: [RouterLink],
    styles: [require('./_main.scss')],
    encapsulation: ViewEncapsulation.None,
    template: require('./main.html')
})

export class MainComponent {

    @ViewChild('code') code: ElementRef;

    ngAfterViewInit() {
        this.initSourceCodeAnim();
    }

    private initSourceCodeAnim() {
        const sourceCodeElement = this.code.nativeElement;
        const spanDefClass = 'annoying';
        const spanDefActiveClass = `${spanDefClass} active`;
        const innerHtml = sourceCodeElement.textContent.split('').map(char => /[a-zA-Z]/.test(char) ? `<span class="${spanDefClass}">${char}</span>` : char).join('');
        sourceCodeElement.innerHTML = innerHtml;
        const children = sourceCodeElement.children;
        let count = 0;
        children[count].className = spanDefClass;

        setInterval(() => {
            children[count].className = spanDefClass;
            count < children.length - 1 ? count++ : count = 0;
            children[count].className = spanDefActiveClass;
        }, SOURCE_CODE_FLASH_INTERVAL);

    }

}
