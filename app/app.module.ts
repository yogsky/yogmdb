import { NgModule }        from '@angular/core';
import { BrowserModule }   from '@angular/platform-browser';
import { AppComponent }    from './app.component';
import { router }          from './app.routes';
import { MainComponent }   from './main/main';
import { TicTacToeModule } from './tictactoe/tic.module';
import { MovieModule }     from './movie/movie.module';

@NgModule({
    declarations: [AppComponent, MainComponent],
    imports: [BrowserModule, router, MovieModule, TicTacToeModule],
    bootstrap:  [AppComponent]
})

export class AppModule {}
