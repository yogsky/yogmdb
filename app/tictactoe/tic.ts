import { Component, ViewEncapsulation, ViewChild } from '@angular/core';

const TABLE_ID = 'tictoe';
const WINNER_DISPLAY_MS = 4500;
const DISABLE_CLASS = 'disabled';
const ENDED_CLASS = `${DISABLE_CLASS} ended`;

type Player = 'x' | 'o';
type BoardPossibilities = Player | '' ;
type Board = BoardPossibilities[];

@Component({
    selector: 'tictactoe',
    styles: [require('./_tic.scss')],
    encapsulation: ViewEncapsulation.None,
    template: `
    <div class="tic-wrapper">
    <h1>Tic-Tac-Yog 💩</h1>
    <h3>currently a dumb random cpu opponent, <br> by next push i'll implement something unbeatable</h3>
    <button class="reset-btn" (click)="setPickPlayerMode()">Reset</button>
    <div class="winner" *ngIf="winner">{{winner}}</div>
    <div *ngIf="shouldShowPicker">
    <h2>Pick one</h2>
    <button class="picker-btn" (click)="userPicked('x')">X</button>
    <button class="picker-btn" (click)="userPicked('o')">O</button>
    </div>
    <div [hidden]="!showTable" class="tic-table-container">
    <table #${TABLE_ID} class="tic-table">
    <tr>
    <td id="0"></td>
    <td id="1"></td>
    <td id="2"></td>
    </tr>
    <tr>
    <td id="3"></td>
    <td id="4"></td>
    <td id="5"></td>
    </tr>
    <tr>
    <td id="6"></td>
    <td id="7"></td>
    <td id="8"></td>
    </tr>
    </table>
    </div>
    </div>

    `
})

export class TicTacToeComponent {
    public cubes: HTMLTableCellElement[] = [];
    public currentPlayer: Player;
    public shouldShowPicker = true;
    private _winner: string;
    private curTimeout: number = null;

    @ViewChild(TABLE_ID) table;

    get showTable() {
        return !this.shouldShowPicker;
    }

    get winner() {
        return this._winner;
    }

    set winner(val: string) {
        if (val) {
            this._winner = val === 'tie' ? 'Its a tie!' : `We got a winner ! congratulations Mr.${val.toUpperCase()}!`;
        } else {
            this._winner = null;
        }
        this.shouldShowPicker = !!!val;
    }

    setPickPlayerMode() {
        clearTimeout(this.curTimeout);
        this.winner = null;
        this.shouldShowPicker = true;
    }

    userPicked(p: Player) {
        this.currentPlayer = p;
        this.switchPlayer();
        this.shouldShowPicker = false;
        this.initTable();
        this.initFirstMove();
    }

    private announceWinnerAndRestart(winner: string) {
        this.winner = winner;
        this.cubes.forEach(d => d.className = ENDED_CLASS);
        this.curTimeout = <any> setTimeout(() => this.setPickPlayerMode(), WINNER_DISPLAY_MS);
    }

    private initFirstMove() {
        const cube = this.cubes[Math.floor(Math.random() * this.cubes.length)];
        this.setCube(cube, this.currentPlayer);
    }

    private switchPlayer() {
        this.currentPlayer = this.currentPlayer === 'x' ? 'o' : 'x';
    }

    private setCube(cube: HTMLTableCellElement, player: Player) {
        cube.innerText = player;
        cube.className = player ? DISABLE_CLASS : '';
    }

    private playerClick(e: MouseEvent) {
        this.switchPlayer();
        const clickedCube = this.cubes.find((d) => d === e.target as HTMLTableCellElement);
        if (clickedCube) {
            if (clickedCube.innerText) {
                return;
            } else {
                this.setCube(clickedCube, this.currentPlayer);
                const winner = this.whoWon(this.getBoard());
                if (winner) {
                    this.announceWinnerAndRestart(winner);
                    return;
                }

                this.generateCPUClick();
            }
        }
    }

    private emptyCubes() {
        return this.cubes.filter((d) => d.innerText === '');
    }

    private getBoard (): Board {
        return <Board> this.cubes.map(d => d.innerText);
    }

    private generateCPUClick() {
        const emptyCubes = this.emptyCubes();
        const board = this.getBoard();

        const randomEmptyCube = emptyCubes[Math.floor(Math.random() * emptyCubes.length)];

        this.switchPlayer();
        this.setCube(randomEmptyCube, this.currentPlayer);

        const winner = this.whoWon(board);

        if (winner) {
            this.announceWinnerAndRestart(winner);
        }
    }

    private initTable() {
        if (this.cubes.length) {
            this.cubes.forEach(d => this.setCube(d, null));
        } else {
            const table = this.table.nativeElement;
            const clickEvent = (e: MouseEvent) => this.playerClick(e);
            table.addEventListener('click', clickEvent);
            const rows = Array.from(table.children[0].children) as HTMLTableRowElement[];
            this.cubes = rows.reduce((rowA, rowB) => rowA.concat(Array.from(rowB.children)), []);
        }
    }

    private whoWon(board: Board): string {

        // check both diagonal possibilities
        if (this.checkWinnerHelper(board, 0, 4, 8)) {
            return this.cubes[0].innerText;
        } else if (this.checkWinnerHelper(board, 2, 4, 6)) {
            return this.cubes[2].innerText;
        }

        // rows top to bottom
        let a = 0;
        let b = 1;
        let c = 2;
        while (c <= board.length) {
            if (this.checkWinnerHelper(board, a, b, c)) {
                return this.cubes[a].innerText;
            }
            a += 3;
            b += 3;
            c += 3;
        }

        a = 0;
        b = 3;
        c = 6;

        // columns left to right
        while (c < board.length) {
            if (this.checkWinnerHelper(board, a, b, c)) {
                return this.cubes[a].innerText;
            }
            a++;
            b++;
            c++;
        }

        if (board.filter(d => d === '').length === 0) {
            return 'tie';
        }

        return '';
    }

    private checkWinnerHelper(board: Board, a: number, b: number, c: number) {
        return board[a] && board[a] === board[b] &&
        board[a] === board[c];
    }
}
