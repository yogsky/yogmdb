import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicTacToeComponent } from './tic';

@NgModule({
    imports: [CommonModule],
    declarations: [TicTacToeComponent]
})

export class TicTacToeModule { }
