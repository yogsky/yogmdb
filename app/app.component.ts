import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'my-app',
    styles: [require('./_app.scss')],
    encapsulation: ViewEncapsulation.None,
    template: `<router-outlet></router-outlet>`
})

export class AppComponent {}
