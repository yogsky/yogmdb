import { Component, ViewChild, Injectable, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { MayAlsoLike } from '../moviemodel';
import * as d3 from 'd3';

const GRAPH_ID = 'maylikegraph';
const LOADER_ID = 'stillloading';
const VALUES_ID = 'valuesmixer';

interface LayoutMayAlsoLike extends MayAlsoLike {
    source: any;
    target: any;
    id: number;
    x?: number;
    y?: number;
    size?: number;
    _children?: any;
}

class Size {
   public static height = 600;
    static get width(): number {
        return window.innerWidth;
    }
}

enum ColorMap {
    Parent = <any> '#CB904D',
    SubParent = <any> '#39A2AE',
    Child = <any> '#437F97'
}

const DefaultsMap = {
    distance: 25,
    linkstrength: 0.9,
    charge: -80
};

type ConfigType = 'NodeLinks' | 'Unique';

@Component({
    selector: 'movie-may-likes',
    styles: [require('./_moviemaylikes.scss')],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    template: require('./moviemaylikes.html')
})

@Injectable()
export class MovieMayLikesComponent {
    private graphSelection: d3.Selection<MayAlsoLike>;
    private svg: d3.Selection<MayAlsoLike>;
    private node: d3.Selection<MayAlsoLike>;
    private link: d3.Selection<MayAlsoLike>;
    private tip: d3.Selection<MayAlsoLike>;
    private data: MayAlsoLike;
    private configList: ConfigType[] = ['NodeLinks', 'Unique'];
    private config = this.configList[0];
    private linkDistance = { id: 'distance', text: 'Distance', value: 25, step: 1, disableOn: ['Unique'] };
    private linkStrength = { id: 'linkstrength', text: 'Link Strength', value: 0.9, step: 0.1, disableOn: ['Unique'] };
    private chargez = { id: 'charge', text: 'Charge', value: -80, step: 1, disableOn: [] };
    private valuesConfig = [this.linkDistance, this.linkStrength, this.chargez];

    @ViewChild(GRAPH_ID) graphElm;
    @ViewChild(LOADER_ID) loadIndicator;
    @ViewChild(VALUES_ID) valuesWrapper;

    applySettings($event?: MouseEvent) {
        this.initAndDraw(this.data);
    }

    disable (disableOn: string[]) {
        return !!disableOn.find(d => this.config === d);
    }

    resetToDefaults() {
        this.valuesConfig.forEach(d => d.value = DefaultsMap[d.id]);
        this.applySettings();
    }

    private removeElements() {
        this.svg.remove();
        this.link.remove();
        this.node.remove();
        this.tip.remove();
    }

    ngAfterViewInit() {
        this.handleConfigVisibility();
    }

    private handleConfigVisibility() {
        [
            { element: this.valuesWrapper.nativeElement, shouldShow: () => this.data ? null : true },
            { element: this.loadIndicator.nativeElement, shouldShow: () => this.data ? true : null }
        ]
        .forEach(d => d3.select(d.element).attr('hidden', d.shouldShow() ));
    }

    initAndDraw(malMasterParent: MayAlsoLike) {
        if (this.svg) {
            this.removeElements();
        }

        this.data = malMasterParent;

        this.handleConfigVisibility();

        this.graphSelection = d3.select(this.graphElm.nativeElement);

        this.svg = this.graphSelection.append('svg')
        .attr({
            width: Size.width,
            height: Size.height
        });

        this.link = this.svg.append('g').selectAll('.link');

        this.node = this.svg.append('g').selectAll('.node');

        this.tip = d3.select(this.graphElm.nativeElement.parentElement).append('div')
            .attr('class', 'tooltip')
            .style('opacity', 0);

        this.draw();
    }

    onChangedConfig(cfg: ConfigType) {
        this.config = cfg;
        this.initAndDraw(this.data);
    }

    private onTickNode(node) {
        node.attr({
            cx: (d) => d.x,
            cy: (d) => d.y
        });
    }

    private onTickLinks(link) {
        link.attr({
            x1: (d) => d.source.x,
            y1: (d) => d.source.y,
            x2: (d) => d.target.x,
            y2: (d) => d.target.y
        });
    }

    private uniqueDataWithSize(parent: any): any {

        return this.flat(parent)
        .sort((a, b) => a.imdbID.localeCompare(b.imdbID))
        .reduce((prev, current, idx, array) => {
            if (prev.find(d => d.imdbID === current.imdbID)) {
                return prev;
            } else {
                current.size = 0;
                let idxB = idx;
                while (idxB < array.length && array[idxB].imdbID === current.imdbID) {
                    current.size++;
                    idxB++;
                }
                prev.push(current);
                return prev;
            }
        }, []);
    }

    private flat (root) {

        const nodes = [];
        let i = 0;

        const recurse = (node) => {
            const children = node.children || node._children;
            if (children) {
                node.isParent = true;
                const shouldBeAdded = !!(node.isParent && node._children) === false;
                if (shouldBeAdded) {
                    children.forEach(recurse);
                }
            }
            node.id = ++i;
            nodes.push(node);
        };

        recurse(root);
        return nodes;
    }

    private color (item, colorScale, length) {
        switch (this.config) {
            case 'NodeLinks':
                if (item.isParent) {
                    // needs work
                    if (item.id === length) {
                        return ColorMap.Parent;
                    } else {
                        return ColorMap.SubParent;
                    }
                } else {
                    return ColorMap.Child;
                }
            case 'Unique':
                const isMaster = item.imdbID === this.data.imdbID;
                return isMaster ? ColorMap.Parent : colorScale(item.size);
            default:
                return [];
        }
    }

    private getColorScale (data: LayoutMayAlsoLike[]) {
        return d3.scale.linear()
            .domain(data.map(d => d.size))
            .range([ColorMap.SubParent, ColorMap.Child]);
    }

    private getData() {
        switch (this.config) {
            case 'NodeLinks':
                return this.flat(this.data);
            case 'Unique':
                return this.uniqueDataWithSize(this.data);
            default:
                return [];
        }
    }

    private nodeSize(data: LayoutMayAlsoLike[], mal: LayoutMayAlsoLike) {
        switch (this.config) {
            case 'NodeLinks':
                return mal.id === data.length ? 30 : mal.children !== null || mal._children ? 20 : 7.5;
            case 'Unique':
                return mal.size * 4;
            default:
                return 10;
        }
    }

    private generateTipHTML(d: LayoutMayAlsoLike) {
        return `<div class="tip-template">
                <p class="info-row title"><span>${d.title}</span></p>
                </br>
                <p class="info-row rating"><span>Rating: ${d.imdbRating}</span></p>
            </div>`;
    }

    private onMouseOver (d: LayoutMayAlsoLike) {
        this.tip.transition()
            .duration(200)
            .style('opacity', .9);

        const imgStyle = d.poster ? `url("${d.poster}")` : null;

        this.tip
            .html(this.generateTipHTML(d))
            .style({
                'left': `${d.x}px`,
                'top': `${d.y - 5}px`
            });

        if (imgStyle) {
            this.tip.style('background-image', imgStyle);
        }

    }

    private onMouseOut() {
        this.tip.transition()
            .duration(100)
            .style('opacity', 0);
    }

    private draw() {

        const layoutData = this.getData();

        const links = d3.layout.tree().links(layoutData);

        const link = this.config === 'NodeLinks' ? this.link.data(links, (d: any) => d.target.id) : null;

        const node = this.node.data(layoutData, (d: any) => d.id);

        const force = d3.layout.force()
            .size([Size.width, Size.height])
            .nodes(layoutData)
            .charge(this.chargez.value)
            .linkStrength(this.linkStrength.value)
            .linkDistance(this.linkDistance.value);

        let onTick;

        if (link) {

            force.links(links);

            link.exit().remove();

            link.enter().append('line')
                .attr('class', 'link')
                .attr('x1', (d: LayoutMayAlsoLike) => d.source.x)
                .attr('y1', (d: LayoutMayAlsoLike) => d.source.y)
                .attr('x2', (d: LayoutMayAlsoLike) => d.target.x)
                .attr('y2', (d: LayoutMayAlsoLike) => d.target.y);

            onTick = () => {
                this.onTickLinks(link);
                this.onTickNode(node);
            };
        } else {
            onTick = () => this.onTickNode(node);
        }

        node.exit().remove();

        const colorScale = this.getColorScale(layoutData);

        node.enter().append('circle')
            .attr('class', 'node')
            .attr('cx', (d: LayoutMayAlsoLike) => d.x)
            .attr('cy', (d: LayoutMayAlsoLike) => d.y)
            .attr('r', (d: LayoutMayAlsoLike) => this.nodeSize(layoutData, d))
            .style('fill', (d: LayoutMayAlsoLike, i: number) => this.color(d, colorScale, layoutData.length))
            .on('click', (d: LayoutMayAlsoLike) => this.onClick(d))
            .on('mouseover', (d: LayoutMayAlsoLike) => this.onMouseOver(d))
            .on('mouseout', () => this.onMouseOut())
            .call(force.drag);

        force
            .on('tick', onTick)
            .start();
    }

    private onClick(d: LayoutMayAlsoLike) {
        if (!d3.event.defaultPrevented) {
            if (d._children) {
                d.children = d._children;
                d._children = null;
                this.initAndDraw(this.data);
            } else if (d.children) {
                d._children = d.children;
                d.children = null;
                this.initAndDraw(this.data);
            }
        }
    }
}
