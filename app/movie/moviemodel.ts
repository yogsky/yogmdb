import { SafeStyle, DomSanitizer } from '@angular/platform-browser';

export type IMovieType = 'movie' | 'series';

export interface IMovieQuery {
  type?: IMovieType;
  text: string;
  byId?: boolean;
  mayLikes?: boolean;
}

export interface IMovieResponse {
    // for some reason their API returns capitalised properties
    // i lower case them in my constructor
    Title : string;
    Year : string;
    Released : string;
    Runtime : string;
    Type : IMovieType;
    Genre: string;
    Director: string;
    Poster: string;
    Plot: string;
    imdbRating: string;
    Metascore: string;
    imdbID: string;
    Error?: string;
  }

  export class Movie {
    public title : string;
    public year : string;
    public released : string;
    public runtime : string;
    public genre: string;
    public director: string;
    public poster: string;
    public plot: string;
    public imdbRating: number;
    public metascore: number;
    public error: string;
    public id: string;
    public sanitizedPosterStyle: SafeStyle;
    public type: IMovieType;

    static isValid(movie: Movie) {
      return !movie.error;
    }

    constructor(res: IMovieResponse, sanitizer: DomSanitizer) {
      if (!res.Error) {
        this.title = res.Title;
        this.year = res.Year;
        this.released = res.Released;
        this.runtime = res.Runtime;
        this.genre = res.Genre;
        this.director = res.Director;
        this.poster = res.Poster;
        this.plot = res.Plot;
        this.imdbRating = Number(res.imdbRating) || 0;
        this.metascore = Number(res.Metascore) || 0;
        this.id = res.imdbID;
        this.error = res.Error;
        this.type = res.Type;

        const isMoviePosterAvailable = !this.poster.toLowerCase().startsWith('n/a');
        if (isMoviePosterAvailable) {
          this.sanitizedPosterStyle = sanitizer.bypassSecurityTrustStyle(`url(${this.poster})`);
        }

      } else {
        this.error = res.Error;
      }
    }

  }

  export interface IMayAlsoLike {
    title: string;
    poster: string;
    imdbID: string;
    imdbRating: number;
    children?: IMayAlsoLike[];
  }

  export class MayAlsoLike implements IMayAlsoLike {
    title: string;
    poster: string;
    imdbID: string;
    imdbRating: number;
    children: MayAlsoLike[];
    constructor(resp: IMayAlsoLike) {
      this.title = resp.title;
      this.poster = resp.poster;
      this.imdbID = resp.imdbID;
      this.imdbRating = resp.imdbRating;
      this.children = resp.children ? (resp.children.map(d => new MayAlsoLike(d))) : null;
    }
  }



