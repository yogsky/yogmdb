import { Component, Injectable, ViewEncapsulation } from '@angular/core';
import { Movie, IMovieQuery } from '../moviemodel';
import { MovieService, MovieUpdateEventObject, MovieUpdateEventType } from '../movieservice';
import { RouterLink } from '@angular/router';

const DEF_TIMEOUT = 5500;
const BODY_LOADING_CLASS_CANCEL_MS = 500;
const BODY_LOADING_CLASS = 'loading';
const BODY_STUCK_CLASS = 'stuck';

@Component({
    selector: 'movies',
    providers: [RouterLink],
    styles: [require('./_moviecontainer.scss')],
    encapsulation: ViewEncapsulation.None,
    template: require('./moviecontainer.html')
})

@Injectable()
export class MovieComponentContainer {
    public message: string = null;
    public titlesConcatenator: string = '';
    private _currentTimeout: number = null;
    private currentMovie: Movie = null;
    private $body = document.body;
    private fetchingStatusDone = true;
    private movies: Movie[] = [];

    constructor(private movieService: MovieService) {
        this.movieService
            .updateEventSubject
            .subscribe((eventInfo: MovieUpdateEventObject) => this.eventRouter(eventInfo));
    }

    private eventRouter(eventInfo: MovieUpdateEventObject) {
        switch (eventInfo.type) {
            case MovieUpdateEventType.ListChanged:
                this.updateList(eventInfo.value);
                break;
            case MovieUpdateEventType.IncomingMovie:
                this.incomingMovie(eventInfo.value);
                break;
            case MovieUpdateEventType.FailedToGetMovie:
                this.incomingMovie(null);
                break;
            case MovieUpdateEventType.FetchingStatusChange:
                this.fetchingStatusChange(eventInfo.value);
                break;
            default:
                break;
        }
    }

    private updateList(movies: Movie[]) {
        this.movies = new Array(...movies);
    }

    queryEvent(query: IMovieQuery) {
        this.movieService.query(query);
    }

    fetchingStatusChange(didFinish) {
        if (this.fetchingStatusDone !== didFinish) {
            this.fetchingStatusDone = didFinish;
            if (this.fetchingStatusDone) {
                setTimeout(() => this.$body.classList.remove(BODY_LOADING_CLASS), BODY_LOADING_CLASS_CANCEL_MS);
            } else {
                this.$body.classList.add(BODY_LOADING_CLASS);
            }
        }
    }

    clickedMovieEvent(movie: Movie) {
        if (movie) {
            this.$body.classList.add(BODY_STUCK_CLASS);
        } else {
            this.$body.classList.remove(BODY_STUCK_CLASS);
        }
        this.currentMovie = movie;
    }

    ngOnDestory() {
        this.movieService.updateEventSubject.unsubscribe();
    }

    private incomingMovie(movie: Movie) {
        this.movies = this.movieService.movies;
        if (movie) {
            this.titlesConcatenator += this.titlesConcatenator.length === 0 ? movie.title : `, ${movie.title}`;
            this.message = `Found ${this.titlesConcatenator} 😊`;
        } else {
            this.message = 'Failed to get the movie 😩';
        }
        clearTimeout(this._currentTimeout);
        this._currentTimeout = null;
        this.clearMessage();
    }

    private clearMessage() {
        this._currentTimeout = <any> setTimeout(() => {
            this.message = null;
            this._currentTimeout = null;
            this.titlesConcatenator = '';
        }, DEF_TIMEOUT);
    }
}
