import { Component, Input, Output, Injectable, ViewEncapsulation,
         EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'movie-item',
    styles: [require('./_movieitem.scss')],
    encapsulation: ViewEncapsulation.None,
    template: require('./movieitem.html'),
    changeDetection: ChangeDetectionStrategy.OnPush
})

@Injectable()
export class MovieItemComponent {
    @Input() movie;
    @Output() deleteMovie = new EventEmitter();
    constructor() { /* */ }
}
