import { Component, Input, Output, Injectable, EventEmitter, ViewChild, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { IMovieQuery, MayAlsoLike, Movie } from '../moviemodel';
import { MovieMayLikesComponent } from '../moviemaylikes/moviemaylikes';
import { MovieService } from '../movieservice';

const CHILD_MAL_ID = 'maylikez';

@Component({
    selector: 'movie-item-popup',
    styles: [require('./_moviedetailspopup.scss')],
    encapsulation: ViewEncapsulation.None,
    entryComponents: [MovieMayLikesComponent],
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: require('./moviedetailspopup.html')
})

@Injectable()
export class MovieItemPopup {
    @Input() movie: Movie;
    @Output() unclickMovie = new EventEmitter();
    @Output() queryMovie = new EventEmitter();
    @ViewChild(CHILD_MAL_ID) maylikesComponent: MovieMayLikesComponent;

    constructor(private movieService: MovieService) {
        movieService
            .incomingMal
            .subscribe((mal: MayAlsoLike) => this.maylikesComponent.initAndDraw(mal));
    }

    ngAfterViewInit() {
        this.queryMovieMayLikes();
    }

    private queryMovieMayLikes() {
        this.movieService.queryMayLikes({text: this.movie.id, byId: true, mayLikes: true });
    }

    emitQuery($event: MouseEvent, mal: MayAlsoLike) {
        $event.stopPropagation();
        const q: IMovieQuery = { text: mal.imdbID, byId: true };
        this.queryMovie.emit(q);
    }

}
