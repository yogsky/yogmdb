import * as d3 from 'd3';
import { Movie } from '../moviemodel';

export type Side = 'upper' | 'lower';
export type AxisMap = { [side: string]: AxisInfo };
export type ScaleMapper = Map<string, d3.scale.Linear<number, number>>
export type SelectionMapper = Map<string, d3.Selection<Movie>>

export const RECT_HEIGHT = 45;
export const RECT_PAD = 110;
export const TEXT_OFFSET_X = 45;
export const TEXT_OFFSET_Y = 35;

export interface AxisInfo {
    key: string;
    transform: string;
    domain: number[];
    text?: {
        transform?: string;
        dx: string;
        dy: string;
        textStr: string;
    };
}

export const GRAPH_ID = 'graph';
export const TEXT_Y_PAD = 10;

export const SVG_PADDING = {
    TOP: 60,
    BOTTOM: 10,
    SIDE: 50
};

export const LEGEND = {
    RECT_SIZE: 30,
    RECT_PAD: 10,
    CONTAINER_SIZE: { HEIGHT: 50, WIDTH: 300 },
};

export const RECT_COLORS = { upper: '#EC7D10', lower: '#FC2F00' };
export const ANIM_DURATION = 500;

export const otherSide: Map<Side, Side> = <Map<Side, Side>> new Map().set('upper', 'lower').set('lower', 'upper');

export class Size {
    static get height(): number {
        return 200;
    }
    static get width(): number{
        return window.innerWidth - SVG_PADDING.SIDE * 2;
    }
}

export class ScaleRange {
    static get value(): number[] {
        return [0 , Size.width];
    }
}

export const AxisInfoMapper = {
    upper: {
        key: 'imdbRating',
        domain: [0, 10],
        textStr: 'iMDB'
    },
    lower: {
        key: 'metascore',
        domain: [0, 100],
        textStr: 'Critics'
    }
};
