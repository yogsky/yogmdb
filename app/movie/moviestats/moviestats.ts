import * as d3 from 'd3';
import { Component, Injectable, ViewEncapsulation, ViewChild, Input } from '@angular/core';
import { Movie } from '../moviemodel';
import * as Stats from './moviestatsdsettings';

const debounce = require('lodash.debounce');

@Component({
    selector: 'movie-stats',
    encapsulation: ViewEncapsulation.None,
    template: `<div #${Stats.GRAPH_ID} class="graph-container"></div>`,
    styles: [require('./_moviestats.scss')]
})

@Injectable()
export class MovieStats {
    @ViewChild(Stats.GRAPH_ID) graphElm;

    private graphSelection: d3.Selection<Movie>;
    private movieTitles: d3.Selection<Movie>;
    private svg: d3.Selection<Movie>;

    private didInit = false;
    private rectValues: Stats.SelectionMapper = <Stats.SelectionMapper> new Map();

    private rects: Stats.SelectionMapper = <Stats.SelectionMapper> new Map();
    private midText: d3.Selection<any>;
    private xScales: Stats.ScaleMapper = <Stats.ScaleMapper> new Map()
        .set('upper', d3.scale.linear()
            .domain(Stats.AxisInfoMapper['upper'].domain)
            .range(Stats.ScaleRange.value))
        .set('lower', d3.scale.linear()
            .domain(Stats.AxisInfoMapper['lower'].domain)
            .range(Stats.ScaleRange.value));

    private resizer = debounce(() => this.initAndDraw(), 200);

    @Input() movies;
    constructor() { /* */ }

    ngOnDestroy() {
        window.removeEventListener('resize', this.resizer);
    }

    ngOnChanges() {
        this.didInit ? this.draw() : this.initAndDraw();
    }

    ngAfterViewInit() {
        this.initAndDraw();

        // on resize: reset all properties then redraw
        // debounce added for better performance
        window.addEventListener('resize', this.resizer);
    }

    private initAndDraw() {

        if (this.svg) {
            this.svg.remove();
        }

        this.graphSelection = d3.select(this.graphElm.nativeElement);

        this.svg = this.graphSelection
            .append('svg')
            .attr('width', Stats.Size.width)
            .attr('height', Stats.Size.height);

        this.rects
            .set('upper', this.svg.append('g'))
            .set('lower', this.svg.append('g'));

        this.rectValues
            .set('upper', this.svg.append('g'))
            .set('lower', this.svg.append('g'));

        this.movieTitles = this.svg.append('g').classed('title', true);

        this.midText = this.svg
            .append('text')
            .attr('transform', `translate(${Stats.Size.width / 2} ,${Stats.Size.height / 2}) rotate(-15)`)
            .classed('info', true);

        const legendG = this.svg.append('g');

        legendG
            .append('rect')
            .classed('legend-container', true)
            .attr({
                width: Stats.LEGEND.CONTAINER_SIZE.WIDTH,
                height: Stats.LEGEND.CONTAINER_SIZE.HEIGHT
            });

        const axisKeys = Object.keys(Stats.AxisInfoMapper);
        for (let i = 0; i < axisKeys.length; i++) {
            legendG
                .append('rect')
                .classed('legend-item', true)
                .attr({
                    width: Stats.LEGEND.RECT_SIZE,
                    height: Stats.LEGEND.RECT_SIZE,
                    fill: Stats.RECT_COLORS[axisKeys[i]],
                    x: (Stats.LEGEND.CONTAINER_SIZE.WIDTH / 2) * i + Stats.LEGEND.RECT_PAD,
                    y: Stats.LEGEND.RECT_PAD
                });

            legendG
                .append('text')
                .attr({
                    x: (Stats.LEGEND.CONTAINER_SIZE.WIDTH / 2) * i + Stats.RECT_PAD / 2,
                    y: Stats.LEGEND.RECT_SIZE
                })
                .text(Stats.AxisInfoMapper[axisKeys[i]].textStr);
        }

        this.didInit = true;
        this.draw();
    }

    private isTwoRects(m: Movie) {
        return m.imdbRating && m.metascore;
    }

    private updateSvgHeight(data: Movie[]) {
        const yPositions = data.map((d, i) => this.rectY(d, i, 'lower'));
        this.svg.attr('height', Math.max(...yPositions) + Stats.RECT_PAD);
    }

    private rectY (d: Movie, i: number, side: Stats.Side) {
        const multi = side === 'lower' || !this.isTwoRects(d) ? 1 : 2;
        const res = i * (Stats.RECT_HEIGHT + Stats.RECT_PAD) + (Stats.RECT_HEIGHT * multi) + Stats.SVG_PADDING.TOP;
        return res;
    };

    private rectTransform (d: Movie, i: number, side: Stats.Side) {
       return `translate(0, ${this.rectY(d, i, side)})`;
    }

    private valuesTransform (d: Movie, i: number, side: Stats.Side) {
        const diff = !this.isTwoRects(d) ? Stats.RECT_HEIGHT / 2 : 0;
        const resY = this.rectY(d, i, side) + diff;
        const width = this.xScales.get(side)(d[Stats.AxisInfoMapper[side].key]);
        return `translate(${width - Stats.TEXT_OFFSET_X / 2}, ${resY + Stats.TEXT_OFFSET_Y})`;
    };

     private titlesTransform (d: Movie, i: number) {
        const padz = !this.isTwoRects(d) ? Stats.RECT_HEIGHT : 0;
        const resY = this.rectY(d, i, 'upper') + padz;
        return `translate(10, ${resY - Stats.RECT_HEIGHT - 5})`;
    };

    draw() {
        const data: Movie[] = this.movies;

        if (!data.length) {
            this.midText.text('Add movie/series for chart to render');
            return;
        }

        this.midText.text('');

        // Draw rects
        this.rects.forEach((rect: d3.Selection<Movie>, side: Stats.Side) => {
            const selectedRect = rect.selectAll('rect').data(data);
            selectedRect.exit().remove();
            selectedRect.enter().append('rect').classed('value-rect', true);
            selectedRect
                .attr('fill', Stats.RECT_COLORS[side])
                .transition()
                .ease('elastic')
                .duration(Stats.ANIM_DURATION)
                .attr('width', (d: Movie, i: number) => this.xScales.get(side)(d[Stats.AxisInfoMapper[side].key]))
                .attr('height', (d: Movie, i: number) => this.isTwoRects(d) ? Stats.RECT_HEIGHT : Stats.RECT_HEIGHT * 2)
                .attr('transform', (d: Movie, i: number) => this.rectTransform(d, i, side));

            const values = this.rectValues.get(side).selectAll('text').data(data);
            values.exit().remove();
            values.enter().append('text');

            values
                .transition()
                .ease('elastic')
                .attr('transform', (d: Movie, i: number) => this.valuesTransform(d, i, side))
                .text((d: Movie) => (d[Stats.AxisInfoMapper[side].key]));

            values.classed('value', true);

            // Not using arrow function in order to the 'this' context
            const mouseOver = function(d: Movie, i: number) {
                d3.select(this)
                    .transition()
                    .duration(Stats.ANIM_DURATION / 2)
                    .ease('linear')
                    .attr('opacity', 0.25);
            };
            const mouseOut = function(d: Movie, i: number) {
                d3.select(this)
                    .transition()
                    .duration(Stats.ANIM_DURATION / 2)
                    .ease('linear')
                    .attr('opacity', 1);
            };

            selectedRect.on('mouseover', mouseOver);
            selectedRect.on('mouseout', mouseOut);

        });

        // Draw movie titles
        const movieTitles = this.movieTitles.selectAll('text').data(data);

        movieTitles.exit().remove();
        movieTitles.enter().append('text');

        movieTitles
            .transition()
            .duration(Stats.ANIM_DURATION)
            .ease('elastic')
            .attr('transform', (d, i) => this.titlesTransform(d, i))
            .text((d) => d.title);

        this.updateSvgHeight(data);
    }
}
