import { Component, Injectable, EventEmitter, Output } from '@angular/core';
import { MovieService } from '../movieservice';
import { MovieSorterPipe } from '../moviesorter/moviesorterpipe';
import { Movie } from '../moviemodel';

@Component({
    selector: 'movie-item-list',
    providers: [MovieSorterPipe],
    styles: [require('./_movieitemlist.scss')],
    template: require('./movieitemlist.html')
})

@Injectable()
export class MovieItemList {
    @Output() clickedMovie = new EventEmitter();
    constructor(public movieService: MovieService) { /* */ }
    movieSorterChange($event: Event) {
        this.movieService.currentSortKey = ($event.target as HTMLOptionElement).value;
    }

    delete(movie: Movie) {
        this.movieService.delete(movie);
    }
}
