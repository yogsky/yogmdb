import { NgModule}                 from '@angular/core';
import { MovieForm }               from './movieform/movieform';
import { MovieItemPopup }          from './moviedetailspopup/moviedetailspopup';
import { MovieMayLikesComponent }  from './moviemaylikes/moviemaylikes';
import { MovieItemComponent }      from './movieitem/movieitem';
import { MovieItemList }           from './movieitemlist/movieitemlist';
import { MovieStats }              from './moviestats/moviestats';
import { CommonModule }            from '@angular/common';
import { MovieComponentContainer } from './moviecontainer/moviecontainer';
import { MovieService }            from './movieservice';
import { HttpModule }              from '@angular/http';
import { FormsModule }             from '@angular/forms';

@NgModule({
    imports: [CommonModule, HttpModule, FormsModule],
    declarations: [MovieComponentContainer,
        MovieForm, MovieItemComponent,
        MovieItemList, MovieStats, MovieItemPopup, MovieMayLikesComponent],
    providers: [MovieService],
})

export class MovieModule {}
