import { Component, Injectable, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { IMovieQuery, IMovieType } from '../moviemodel';

const TITLE_INPUT_ID = 'title-input';

@Component({
    selector: 'movie-form',
    encapsulation: ViewEncapsulation.None,
    styles: [require('./_movieform.scss')],
    template: require('./movieform.html')
})

@Injectable()
export class MovieForm {
    public query: IMovieQuery = { text: null, type: 'movie' };
    public queryTypes: IMovieType[] = ['movie', 'series'];
    private $titleInput: HTMLElement = null;
    @Output() queryEvent = new EventEmitter();
    constructor() { /* */ }
    ngAfterViewInit() {
        this.$titleInput = document.getElementById(TITLE_INPUT_ID);
    }

    public onSubmit($event: MouseEvent) {
        $event.preventDefault();
        this.$titleInput.blur();
        this.performQuery();
    }

    public addYogHorrorList() {
        this.query = { text: 'The Exorcist, The Ring, The Conjuring, The Sixth Sense, The Orphanage', type: 'movie' };
        this.performQuery();
    }

    private performQuery() {
        let values = this.query.text.split(',');
        for (let value of values) {
            value = value.trim();
            const q: IMovieQuery = { text: value, type: this.query.type };
            this.queryEvent.emit(q);
        }
        this.query.text = null;
    }

}
