import { Pipe, PipeTransform} from '@angular/core';
import { Movie } from '../moviemodel';

@Pipe({ name: 'movieSorter' })

/*
  YOG-NOTE ->
  this was originally used in template, decided to drop this from template and use this
  directly @ getter in movie service
  */

  export class MovieSorterPipe implements PipeTransform {
      transform(movies: Movie[], key: string): Movie[] {
          let sorter: (movieA: Movie, movieB: Movie) => number;
          switch (key) {
              case 'title':
              sorter = (movieA: Movie, movieB: Movie) => movieA.title.localeCompare(movieB.title);
              break;
              case 'metascore':
              sorter = (movieA: Movie, movieB: Movie) => movieA.metascore - movieB.metascore;
              break;
              case 'imdbRating':
              sorter = (movieA: Movie, movieB: Movie) => movieA.imdbRating - movieB.imdbRating;
              break;
              default:
              sorter = null;
              break;
          }
          return movies.sort(sorter);
      }
  }
