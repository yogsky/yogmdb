import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { MovieSorterPipe } from './moviesorter/moviesorterpipe';
import { Movie, IMovieQuery, MayAlsoLike } from './moviemodel';
import { DomSanitizer } from '@angular/platform-browser';

export enum MovieUpdateEventType {
  IncomingMovie = 1,
  FailedToGetMovie = 2,
  ListChanged = 3,
  FetchingStatusChange = 4
}

export interface MovieUpdateEventObject {
  type: MovieUpdateEventType;
  value?: any;
}

@Injectable()
export class MovieService {
    private _movies: Movie[] = [];
    public sortKeysMap = new Map<string, string>()
      .set('Title', 'title')
      .set('IMDB Rating', 'imdbRating')
      .set('Metacritics', 'metascore');
    private _currentSortKey: string = this.sortKeysMap.get(Array.from(this.sortKeysMap.keys())[0]);
    private updateSubject: Subject<MovieUpdateEventObject> = new Subject<MovieUpdateEventObject>();
    private malUpdateSubject: Subject<any> = new Subject<any>();
    private _currentlyFetching = 0;
    private _sorter = new MovieSorterPipe();

    constructor(private _http: Http, private _sanitizer: DomSanitizer) { /* noop */ }

    query(query: IMovieQuery) {
      if (this._currentlyFetching === 0) {
        this.updateSubject.next({type: MovieUpdateEventType.FetchingStatusChange, value: false});
      }

      this._currentlyFetching++;
      // Please note I'm new to Rxjs, still unaware of best practices
      // for now I expose different subjects/events to whichever component wants to subscribe

      // 1. map to a new movie class instance
      // 2. if invalid, fire event (i'm using this event to show the user that the movie wasnt found)
      // 3. skip the event action if already in list or invalid
      // 4. pushing to list, fire events
      this._http.get(this.generateTitleQueryUrl(query))
        .do(() => this.handleNextFetchedObject())
        .map((d: any) => new Movie(d.json(), this._sanitizer)) // 1
        .do((movie: Movie) => !Movie.isValid(movie) ? this.updateSubject.next({type: MovieUpdateEventType.FailedToGetMovie}) : null) // 2
        .skipWhile((movie: Movie) => !Movie.isValid(movie) || this.isMovieInList(movie)) // 3
        .subscribe((movie: Movie) => this.incomingMovieAction(movie)); // 4
    }

    queryMayLikes(query: IMovieQuery) {
      this._http.get(this.generateMayLikesQueryUrl(query))
        .do(() => this.handleNextFetchedObject())
        .map((d: any) => new MayAlsoLike(d.json()))
        .subscribe((mal: MayAlsoLike) => this.incomingMayAlsoLikeAction(mal));
    }

    private incomingMayAlsoLikeAction(mal: MayAlsoLike) {
      this.malUpdateSubject.next(mal);
    }

    private incomingMovieAction(incomingMovie: Movie) {
      this._movies.push(incomingMovie);
      this.updateSubject.next({ type: MovieUpdateEventType.IncomingMovie, value: incomingMovie });
      this.updateSubject.next({ type: MovieUpdateEventType.ListChanged, value: this.movies });
    }

    private handleNextFetchedObject() {
      this._currentlyFetching--;
      if (this._currentlyFetching === 0) {
        this.updateSubject.next({type: MovieUpdateEventType.FetchingStatusChange, value: true});
      }
    }

    private isMovieInList(incomingMovie: Movie) {
      return this._movies.findIndex((movie: Movie) => movie.id === incomingMovie.id) > -1;
    }

    private generateTitleQueryUrl(query: IMovieQuery) {
      if (!query.byId) {
        return `/yog/title/?title=${query.text.replace(/\s+/g, '+')}&type="${query.type}"`;
      } else {
        return `/yog/title/?imdbID=${query.text}`;
      }
    }

    private generateMayLikesQueryUrl(query: IMovieQuery) {
      return `/yog/maylikes/?imdbID=${query.text}`;
    }

    delete(movie: Movie) {
        this._movies = this._movies.filter(d => d.id !== movie.id);
        this.updateSubject.next({ type: MovieUpdateEventType.ListChanged, value: this.movies });
    }

    get movies() {
      // exposing a COPY of the list instance,
      // (not the real instance that is private to this service)
      // after sorting that copy with the given key
      return this._sorter.transform(this._movies, this._currentSortKey);
    }

    // EVENTS //
    get updateEventSubject () {
      return this.updateSubject;
    }

    get incomingMal () {
      return this.malUpdateSubject;
    }

    // SORT KEY //
    set currentSortKey(key: string) {
      this._currentSortKey = this.sortKeysMap.get(key);
      // taking advantage of the set action to raise the change event
      this.updateSubject.next({ type: MovieUpdateEventType.ListChanged, value: this.movies });
    }

    get currentSortKey() {
      return this._currentSortKey;
    }

    get sortKeys() {
      return Array.from(this.sortKeysMap.keys());
    }

}
