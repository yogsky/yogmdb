let webpack = require('webpack');
let path = require('path');

module.exports = {
  name: 'client',
  entry: {
    'vendor': './app/vendor',
    'app': './app/boot'
},
output: {
    path: __dirname,
    filename: './dist/[name].bundle.js'
},
resolve: {
    extensions: ['', '.ts', '.js']
},
devtool: 'eval-source-map',
module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: "source-map-loader"
      }
    ],
    loaders: [
    {
        test: /\.ts/,
        loaders: ['ts-loader'],
        exclude: /node_modules/
    },
    {
        test: /\.scss$/,
        loaders: ['raw-loader', 'sass-loader'],
        exclude: /node_modules/
    },
    {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: 'url-loader',
        exclude: /node_modules/
    },
    {
        test: /\.svg$/,
        loader: 'svg-inline?classPrefix'
    },
    {
        test: /\.html$/,
        loader:`html?-minimize`
    },
    {
        test: /\.json$/,
        loader: 'json-loader'
    }
    ]
},
htmlLoader: {
    ignoreCustomFragments: [/\{\{.*?}}/]
},
plugins: [
new webpack.HotModuleReplacementPlugin(),
new webpack.optimize.CommonsChunkPlugin(/* chunkName= */'vendor', /* filename= */'./dist/vendor.bundle.js'),
    // new webpack.optimize.UglifyJsPlugin({
    //   mangle: false,
    //   comments: false
    // })
    ]
}
